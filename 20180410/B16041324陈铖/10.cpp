#include<stdio.h>
#include<string.h>

char *WordChange(char *r){
 	char t=*r;
 	while(1){
         if(t=='\n')
             break;
         t-=32;
 	}
 	return t;
}

char *AntiSort(char *r){
	int i=0;
	int j=strlen(r);
 	char flag;
 	while(i<j){
 		flag=r[i];
 		r[i]=r[j];
 		r[j]=flag;
 		i++;
 		j--;
 	}
}

char *MoveRight(char *r){
	char t=*r;
    while(1){
       if(t=='\n')
            break;
        t-=2;
        if(t==65)//A->X
       	t='X';
        else if(t==66)//B->Y
            t='Y';
        else if(t==67)//C->Z
            t='Z';
        else if(t==97)//a->x
        	t='x';
        else if(t==98)//b->y
        	t='y';
        else if(t==99)//c->z
            t='z';
    }
    return t;
}


int main()
{
	char r[50];
 	gets(r);
 	WordChange(r);
 	AntiSort(r);
 	MoveRight(r);
 	puts(r);
 	return 0;
 } 
