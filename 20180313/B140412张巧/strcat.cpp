// 数据排序 --- 插入法排序
#include <iostream>
using namespace std;

int main()
{
	int i, j, tmp;
	int a[]={3,4,1,2,6,5}, n=6;
	
	for(i=1; i<n; i++)
	{
		tmp=a[i];
		
        for(j=i-1; j>=0; j--)
        	if(a[j]>tmp)
        		a[j+1]=a[j];	// 往后移动一个位置
			else
				break; 
        	
        a[j+1]=tmp;
        
        for(int k=0; k<n; k++)
			cout<<a[k]<<" ";
		cout<<endl;
	}
	
	for(i=0; i<n; i++)
		cout<<a[i]<<" ";
	cout<<endl;
	
	return 0;
}

/*
3,4,1,2,6,5
          i
1 3 4     2 6 5
    3     4
j
          
          tmp=a[i];
          ........
          a[j+1]=tmp;
          
*/

