## strcat

test.c 为函数测试程序源码,直接运行结果:

```
$ ./test 
input:
hello
world
output:
helloworld

```


也可以直接引用 str2.h头文件进行测试

```
//函数原型 char * strcat(char * str1, char * str2)
#include <stdio.h>
#include "str2.h"
int main(){
	puts("input:");
	char a[100];
	char b[100];
	scanf("%s",a);
	scanf("%s",b);
	char *c =strcat(a,b);
	puts("output:");
	puts(c);
	return 0;
}
```
