#include<stdio.h>
int copyfile(const char **argv)
{
	FILE *fp1=fopen(argv[1],"r");
	FILE *fp2=fopen(argv[2],"w");
	if(fp1==NULL||fp2==NULL)
		return 0;
	do
	{
		char x1=fgetc(fp1);
		if(feof(fp1))
			return 1;
		fputc(x1,fp2);
	}while(1);
}
int main(int argc,const char **argv)
{
	copyfile(argv);
	return 0;
}
