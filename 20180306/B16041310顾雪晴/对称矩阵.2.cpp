#include <iostream>
#include <iomanip>
using namespace std;
#define M 7
void Init(int **a)
{
  for(int m=0;m<M;m++)
    for(int n=0;n<M;n++)
		a[m][n]=0;
}
void Change1(int **a)
{
	int j=0;
	int k=1;
  for(int m=0;m<M;m++)
  {
	 for(int n=0;n<=j;n++)
	 {
	     a[m][n]=a[n][m]=k++;
	 }
	 j++;
  }
}
void Change2(int **b)
{
	int count=1;
     for(int i=0;i<M;i++)
	 {
	    for(int j=i;j<M;j++)
		{
			b[i][j]=b[j][i]=count++;
		}
	 }
}
void Print(int **a)
{
  for(int m=0;m<M;m++)
  {
    for(int n=0;n<M;n++)
       cout<<setw(5)<<a[m][n];
	cout<<endl;
  }
}
int main()
{
	int **a;
	int **b;
	a=new int*[M];
	b=new int*[M];
	for(int i=0;i<M;i++)
	{
		a[i]=new int[M];
        b[i]=new int[M];
	}
	Init(a);
	Change1(a);
	Init(b);
	Change2(b);
	Print(a);
	cout<<endl;
	Print(b);
	for(int j=0;j<M;j++)
	{
		delete[] a[j];
		delete[] b[j];
	}
	delete[] a;
	delete[] b;
  return 0;
}