#include<stdio.h>
int Shuhe_for(int x)
{
    int sum = 0;
    int n = x;
    for (; n; n /= 10)
        sum += n % 10;
    return sum;
}

int Shuhe_while(int x)
{
    int sum = 0;
    int n = x;
    while (n)
    {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}

int Shuhe_doWhile(int x)
{
    int sum = 0;
    int n = x;
    do
    {
        sum += n % 10;
        n /= 10;
    } while (n);
    return sum;
}

int Shuhe_whileTrue(int x)
{
    int sum = 0;
    int n = x;
    while(1)
    {
        sum += n % 10;
        n /= 10;
        if (!n)
            break;
    }
    return sum;
}
int main(void){
	int n;
	scanf("%d",&n);
	 printf("%d\n", Shuhe_for(n));
    printf("%d\n", Shuhe_while(n));
    printf("%d\n", Shuhe_doWhile(n));
    printf("%d\n", Shuhe_whileTrue(n));
    return 0;
}
	

