#include<stdio.h>

int wayfor(int x)
{
	int sum=0;
	for(int i=1;x;i++)
	{
		sum+=x%10;
		x=x/10;
	}
	return sum;
}

int waywhile(int x)
{
	int sum=0;
	while(x)
	{
		sum+=x%10;
		x=x/10;
	}
	return sum;
}

int waydowhile(int x)
{
	int sum=0;
	do
	{
		sum+=x%10;
		x=x/10;
	}while(x);
	return sum;
}

int waywhile1(int x)
{
	int sum=0;
	while(1)
	{
		sum+=x%10;
		x=x/10;
		if(!x) break;
	}
	return sum;
}

int main()
{
	int x;
	int sum;
	int a;
	printf("请输入要计算的数x:  ");
	scanf("%d",&x);
	printf("选择计算方法： ");
	scanf("%d",&a);
	if(a==1)
	{
	   sum=wayfor(x);
    }
    if(a==2)
    {
    	sum=waywhile(x);
	}
	if(a==3)
	{
		sum=waydowhile(x);
	}
	if(a==4)
	{
		sum=waywhile1(x);
	}
	printf("%d",sum);
	return 0;
}
